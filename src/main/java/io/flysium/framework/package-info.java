
/**
 * SSM框架核心代码，一般不建议随意修改。
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 1.0
 */
package io.flysium.framework;