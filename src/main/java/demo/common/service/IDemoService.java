package demo.common.service;

import demo.common.vo.DemoVO;

/**
 * Service样例接口
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 2017年3月2日
 */
public interface IDemoService {

	/**
	 * 欢迎
	 * 
	 * @param vo
	 * @return
	 */
	public String sayHi(DemoVO vo);

}
